/*! Copyright(c) 2017 https://github.com/taengstagram

This software is released under the Reciprocal Public License 1.5.
https://opensource.org/licenses/RPL-1.5
*/
(function () {

    const mpdFormEle = document.getElementById('mpdForm');
    const mpdInputEle = document.getElementById('mpdInput');
    const playerContainerEle = document.getElementById('videoContainer');
    const noticeEle = document.getElementById('notice');
    const video = document.getElementById('player');
    const uiConfig = {
        addBigPlayButton: true,
        addSeekBar: false,
        controlPanelElements: [
            'play_pause', 'mute', 'volume', 'spacer', 'quality_label', 'fullscreen', 'overflow_menu'],
        overflow_menu: ['cast', 'quality', 'picture_in_picture']
    };

    if (!!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform)) {
        noticeEle.innerText = "iOS may not be supported";
    }

    // Custom "Live" text element
    class LiveIndicator extends shaka.ui.Element {
        constructor(parent, controls) {
            super(parent, controls);

            // The actual button that will be displayed
            this.button_ = shaka.util.Dom.createHTMLElement('button');
            this.button_.textContent = '';
            this.button_.classList.add('custom-live-indicator');
            this.parent.appendChild(this.button_);

            this.eventManager.listen(this.player, 'streaming', () => {
                this.setButtonLabel_();
            });

            this.eventManager.listen(this.video, 'playing', () => {
                this.setButtonLabel_();
            });
        }

        setButtonLabel_() {
            if (!this.player.isLive()) {
                shaka.ui.Utils.setDisplay(this.button_, false);
                return;
            }
            if (this.button_.textContent !== 'Live') {
                this.button_.textContent = 'Live';
                this.button_.classList.add('custom-live-indicator-is-live');
            }
        }
    };

    // Factory that will create a button at run time.
    LiveIndicator.Factory = class {
        create(rootElement, controls) {
            return new LiveIndicator(rootElement, controls);
        }
    };

    // Register our factory with the controls, so controls can create button instances.
    shaka.ui.Controls.registerElement('live', new LiveIndicator.Factory());

    class QualityIndicator extends shaka.ui.Element {

        constructor(parent, controls) {
            super(parent, controls);

            // The actual button that will be displayed
            this.button_ = shaka.util.Dom.createHTMLElement('button');
            this.button_.textContent = '';
            this.button_.classList.add('custom-quality-indicator');
            this.parent.appendChild(this.button_);

            // Ref: https://shaka-player-demo.appspot.com/docs/api/shaka.Player.html#.event:AdaptationEvent
            this.eventManager.listen(this.player, 'adaptation', () => {
                this.setButtonLabel_();
            });

            // Ref: https://shaka-player-demo.appspot.com/docs/api/shaka.Player.html#.event:VariantChangedEvent
            this.eventManager.listen(this.player, 'variantchanged', () => {
                this.setButtonLabel_();
            });

            this.eventManager.listen(this.player, 'abrstatuschanged', () => {
                this.setButtonLabel_();
            });
        }

        setButtonLabel_() {
            let tracks = this.player.getVariantTracks();
            if (tracks.length == 1) {
                // don't show "Auto" if only 1 track
                this.button_.innerHTML = '<span>' + this.formatLabel_(tracks[0], true) + '</span>';
                return;
            }

            if (this.player.getConfiguration().abr.enabled) {
                this.button_.classList.add('abr');
            } else {
                this.button_.classList.remove('abr');
            }

            let track = this.getActiveTrack_(tracks);
            const newLabel = '<span>' + this.formatLabel_(track) + '</span>';
            if (newLabel !== this.button_.innerHTML) {
                // set only when changed so that animation is not jerky
                this.button_.innerHTML = '<span>' + this.formatLabel_(track) + '</span>';
            }
        }

        getActiveTrack_(tracks) {
            for (let index = 0; index < tracks.length; index++) {
                const track = tracks[index];
                if (track.active) {
                    return track;
                }
            }
            return null;
        }

        formatLabel_(track, suppressAuto) {
            if (track == null) {
                return '';
            }
            const res = (track.label == null ? track.height + "P" : track.label);
            if (this.player.getConfiguration().abr.enabled && !suppressAuto) {
                return 'Auto ' + res;
            }
            return res;
        }
    };

    // Factory that will create a button at run time.
    QualityIndicator.Factory = class {
        create(rootElement, controls) {
            return new QualityIndicator(rootElement, controls);
        }
    };

    // Register our factory with the controls, so controls can create button instances.
    shaka.ui.Controls.registerElement('quality_label', new QualityIndicator.Factory());

    function getHashParams() {
        return window.location.hash.substr(1).split('&').reduce(function (result, item) {
            var parts = item.split('=');
            result[parts[0]] = parts[1];
            return result;
        }, {});
    }

    // get current manifest uri
    function getManifestUri() {
        // try to get value from input
        if (mpdInputEle.value.trim() !== '') {
            window.location.hash = '#mpd=' + encodeURIComponent(mpdInputEle.value.trim());
            mpdFormEle.style.display = 'none';
            playerContainerEle.style.display = 'flex';
            mpdInputEle.value = ''; // clear
        }

        var hashParams = getHashParams();

        if (mpdInputEle.value.trim() === ''
            && (hashParams['mpd'] === undefined || hashParams['mpd'] === '')) {
            return '';
        }

        return (mpdInputEle.value !== ''
            ? mpdInputEle.value.trim()
            : decodeURIComponent(hashParams['mpd']).trim()
        );
    }

    function getPosterUri() {
        var poster = '';    // default
        var hashParams = getHashParams();
        if (hashParams['poster'] !== undefined && hashParams['poster'] !== '') {
            poster = decodeURIComponent(hashParams['poster']).trim();
        }
        return poster;
    }

    function initPlayer() {

        const manifestUri = getManifestUri();
        if (manifestUri === undefined || manifestUri === '') {
            console.warn('No mpd stream defined.');
            playerContainerEle.style.display = 'none';
            mpdFormEle.style.display = 'block';
            return;
        }

        video.setAttribute('poster', getPosterUri());

        // Create a Player instance.
        const ui = video['ui'];
        ui.configure(uiConfig);
        const controls = ui.getControls();
        const player = controls.getPlayer();

        player.addEventListener(
            'buffering',
            function () {
                if (player.isBuffering()) {
                    checkBroadcastEnded(manifestUri);
                }
            }
        );

        // Listen for error events.
        player.addEventListener('error', onPlayerErrorEvent);
        controls.addEventListener('error', onUIErrorEvent);

        // Try to load a manifest.
        // This is an asynchronous process.
        player.load(manifestUri).then(function () {

            // add appropriate control depending on stream nature
            uiConfig.controlPanelElements.splice(1, 0, (player.isLive() ? 'live' : 'time_and_duration'));
            ui.configure(uiConfig);

            // // Manually select best quality track
            // let tracks = player.getVariantTracks();
            // if (tracks.length && tracks[0].bandwidth) {
            //     tracks.sort(function (a, b) {
            //         // sort in descending quality
            //         return b.bandwidth - a.bandwidth;
            //     });
            //     const track = tracks[0];    // best quality

            //     // Ref: https://shaka-player-demo.appspot.com/docs/api/ui_resolution_selection.js.html#line187
            //     // onTrackSelected_(track) {}
            //     // // Disable abr manager before changing tracks.
            //     let config = { abr: { enabled: false } };
            //     player.configure(config);
            //     const clearBuffer = controls.getConfig().clearBufferOnQualityChange;
            //     player.selectVariantTrack(track, clearBuffer);
            // }

            // This runs if the asynchronous load is successful.
            var promise = video.play();
            if (promise !== undefined) {
                promise.catch(function (error) {
                    // force play button to show
                    controls.getControlsContainer().setAttribute("shown", "true");
                    // video.muted = true;
                    // video.play();
                });
            }
        }).catch(onPlayerError);  // onError is executed if the asynchronous load fails.
    }

    function stopStreaming() {
        noticeEle.innerText = "Stream has ended.";
        video.setAttribute('poster', 'error.jpg');
        video['ui'].getControls().getPlayer().unload();
        alert("Stream has ended.");
    }

    function checkBroadcastEnded(manifestUri) {
        var client = new XMLHttpRequest();
        client.open('HEAD', manifestUri, true);
        client.send();

        client.onreadystatechange = function () {
            if (this.readyState == this.HEADERS_RECEIVED) {
                var broadcastEnded = client.getResponseHeader('x-fb-video-broadcast-ended');
                if (broadcastEnded !== null && broadcastEnded === '1') {
                    stopStreaming();
                    return;
                }
                var lastModifiedHeader = client.getResponseHeader('last-modified');
                if (video['ui'].getControls().getPlayer().isLive() && lastModifiedHeader !== null) {
                    var lastModified = new Date(lastModifiedHeader);
                    if ((new Date() - lastModified) > (1000 * 60 * 3)) {
                        // manifest last modified > 3 minutes ago
                        stopStreaming();
                        return;
                    }
                }
            }
        }
    }

    function onPlayerErrorEvent(errorEvent) {
        // Extract the shaka.util.Error object from the event.
        onPlayerError(event.detail);
    }

    function onPlayerError(error) {
        // Handle player error
        console.error('Error code', error.code, 'object', error);
        video.setAttribute('poster', 'error.jpg');
        alert("Unable to load video. \nStream may have ended.");
    }

    function onUIErrorEvent(errorEvent) {
        // Extract the shaka.util.Error object from the event.
        onPlayerError(event.detail);
    }

    function initFailed() {
        // Handle the failure to load
        console.error('Unable to load the UI library');
        noticeEle.innerText = "Unable to load the UI library";
    }

    document.addEventListener('shaka-ui-loaded', initPlayer);
    document.addEventListener('shaka-ui-load-failed', initFailed);
    mpdFormEle.addEventListener('submit', initPlayer);
})();
