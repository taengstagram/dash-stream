/*! Copyright(c) 2017 https://github.com/taengstagram

This software is released under the Reciprocal Public License 1.5.
https://opensource.org/licenses/RPL-1.5
*/
(function () {
    const mpdFormEle = document.getElementById('mpdForm');
    const mpdInputEle = document.getElementById('mpdInput');
    const videoContainerEle = document.getElementById('videoContainer');

    if (!!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform)) {
        var notice = document.getElementById('notice');
        notice.innerText = "iOS may not be supported";
    }

    function loadMpd() {
        var poster = '';     // default

        // try to get value from input
        if (document.getElementById('mpdInput').value.trim() !== '') {
            window.location.hash = '#mpd=' + encodeURIComponent(document.getElementById('mpdInput').value.trim());
            mpdFormEle.style.display = 'none';
            videoContainerEle.style.display = 'block';
            mpdInputEle.value = ''; // clear
        }

        const hashParams = window.location.hash.substr(1).split('&').reduce(function (result, item) {
            var parts = item.split('=');
            result[parts[0]] = parts[1];
            return result;
        }, {});

        if (mpdInputEle.value.trim() === ''
            && (hashParams['mpd'] === undefined || hashParams['mpd'] === '')) {
            console.warn('No mpd stream defined.');
            videoContainerEle.style.display = 'none';
            mpdFormEle.style.display = 'block';
            return;
        }

        const mpdUrl = (
            mpdInputEle.value !== ''
                ? document.getElementById('mpdInput').value.trim()
                : decodeURIComponent(hashParams['mpd']).trim()
        );

        if (hashParams['poster'] !== undefined && hashParams['poster'] !== '') {
            poster = decodeURIComponent(hashParams['poster']).trim();
        }
        const player = videojs("player", { responsive: true });
        player.on('error', function () {
            this.poster('error.jpg');
            this.removeClass('vjs-looping');
            alert("Unable to load video. \nStream may have ended.");
        });
        player.ready(function () {
            if (poster !== '') {
                this.poster(poster);
            }
            this.src({
                src: mpdUrl,
                type: 'application/dash+xml'
            });
            this.play();
        });
    }

    mpdFormEle.addEventListener('submit', loadMpd);
    loadMpd();
})();
