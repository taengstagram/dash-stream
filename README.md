# dash-stream

![Build Status](https://gitlab.com/taengstagram/dash-stream/badges/master/build.svg)


# Examples

## [videojs](https://github.com/videojs/video.js/)

- [No Stream](https://taengstagram.gitlab.io/dash-stream/video.html)
- [Working Stream](https://taengstagram.gitlab.io/dash-stream/video.html#mpd=https%3A%2F%2Flivesim.dashif.org%2Flivesim%2Ftestpic_2s%2FManifest.mpd)
- [Working Stream with Custom Poster](https://taengstagram.gitlab.io/dash-stream/video.html#mpd=https%3A%2F%2Flivesim.dashif.org%2Flivesim%2Ftestpic_2s%2FManifest.mpd&poster=https%3A%2F%2Fdummyimage.com%2F600x800%2FFFFFFF%2F333333.jpg%26text%3DCustom%2BPoster)
- [Error Stream](https://taengstagram.gitlab.io/dash-stream/video.html#mpd=https%3A%2F%2Fhttpbin.org%2Fstatus%2F410)
- [Error Stream with Custom Poster](https://taengstagram.gitlab.io/dash-stream/video.html#mpd=https%3A%2F%2Fhttpbin.org%2Fstatus%2F410&poster=https%3A%2F%2Fdummyimage.com%2F600x800%2FFFFFFF%2F333333.jpg%26text%3DCustom%2BPoster)

## [shaka-player](https://github.com/google/shaka-player/)

- [No Stream](https://taengstagram.gitlab.io/dash-stream/shaka.html)
- [Working Stream](https://taengstagram.gitlab.io/dash-stream/shaka.html#mpd=https%3A%2F%2Flivesim.dashif.org%2Flivesim%2Ftestpic_2s%2FManifest.mpd)
- [Working Stream with Custom Poster](https://taengstagram.gitlab.io/dash-stream/shaka.html#mpd=https%3A%2F%2Flivesim.dashif.org%2Flivesim%2Ftestpic_2s%2FManifest.mpd&poster=https%3A%2F%2Fdummyimage.com%2F600x800%2FFFFFFF%2F333333.jpg%26text%3DCustom%2BPoster)
- [Error Stream](https://taengstagram.gitlab.io/dash-stream/shaka.html#mpd=https%3A%2F%2Fhttpbin.org%2Fstatus%2F410)
- [Error Stream with Custom Poster](https://taengstagram.gitlab.io/dash-stream/shaka.html#mpd=https%3A%2F%2Fhttpbin.org%2Fstatus%2F410&poster=https%3A%2F%2Fdummyimage.com%2F600x800%2FFFFFFF%2F333333.jpg%26text%3DCustom%2BPoster)
- [Multi-Resolution Stream](https://taengstagram.gitlab.io/dash-stream/shaka.html#mpd=https%3A%2F%2Fdash.akamaized.net%2Fdash264%2FTestCases%2F2b%2Fqualcomm%2F1%2FMultiResMPEG2.mpd)
